<?php

namespace App\Controller;

use App\Entity\Bagues;
use App\Entity\BoucleDor;
use App\Entity\Bracelets;
use App\Entity\Colliers;
use App\Entity\Size;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;


class BijouxController extends Controller
{
    public function __construct() {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });

        $this->serializer = new Serializer(array($normalizer), array($encoder));
    }
    /**
     * @Route("/home", name="home")
     */
    public function index()
    {
        return $this->render('bijoux/index.html.twig', [
            'controller_name' => 'BijouxController',
        ]);
    }
    /**
     * @Route("/bagues", name="bagues")
     */
    public function showBagues()
    {
        $bagues = $this->getDoctrine()
            ->getRepository(Bagues::class)
            ->findAll();

        $data = $this->serializer->serialize($bagues, 'json');

        return new Response($data);
    }
    /**
     * @Route("/bracelets", name="bracelets")
     */
    public function showBracelets()
    {
        $bracelets = $this->getDoctrine()
            ->getRepository(Bracelets::class)
            ->findAll();

        $data = $this->serializer->serialize($bracelets, 'json');

        return new Response($data);
    }
    /**
     * @Route("/colliers", name="colliers")
     */
    public function showColliers()
    {
        $colliers = $this->getDoctrine()
            ->getRepository(Colliers::class)
            ->findAll();

        $data = $this->serializer->serialize($colliers, 'json');

        return new Response($data);
    }
    /**
     * @Route("/boucles", name="boucles")
     */
    public function showBoucleDor()
    {
        $boucles = $this->getDoctrine()
            ->getRepository(BoucleDor::class)
            ->findAll();

        $data = $this->serializer->serialize($boucles, 'json');

        return new Response($data);
    }
    /**
     * @Route("/bague/{id}", name="BagueId")
     */
    public function showBague($id)
    {
        $bague = $this->getDoctrine()
            ->getRepository(Bagues::class)
            ->find($id);


        $data = $this->serializer->serialize($bague, 'json');

        return new Response($data);
    }
    /**
     * @Route("/boucle/{id}", name="boucleID")
     */
    public function showBoucle($id)
    {
        $boucle = $this->getDoctrine()
            ->getRepository(BoucleDor::class)
            ->find($id);


        $data = $this->serializer->serialize($boucle, 'json');

        return new Response($data);
    }
    /**
     * @Route("/collier/{id}", name="collierID")
     */
    public function showCollier($id)
    {
        $collier = $this->getDoctrine()
            ->getRepository(Colliers::class)
            ->find($id);


        $data = $this->serializer->serialize($collier, 'json');

        return new Response($data);
    }
    /**
     * @Route("/bracelet/{id}", name="BraceletID")
     */
    public function showBracelet($id)
    {
        $bracelet = $this->getDoctrine()
            ->getRepository(Bracelets::class)
            ->find($id);

        $data = $this->serializer->serialize($bracelet, 'json');

        return new Response($data);
    }
    /**
     * @Route("/size", name="size")
     */
    public function addSize(Request $request){

        $entityManager = $this->getDoctrine()->getManager();

        $size = $this->getDoctrine()
            ->getRepository(Size::class)
            ->findAll();

        $data = $request->getContent(); $data = json_decode($data , true);

        $size = new Size();
        $size->setSize($data['size']);

        $entityManager->persist($size);
        $entityManager->flush();

        return new JsonResponse('yeaaaaah');
    }
    /**
     * @Route("/dataSerialize", name="dataSerialize")
     */
    public function dataSerialize()
    {
        $size = $this->getDoctrine()
            ->getRepository(Size::class)
            ->findAll();

        $data = $this->serializer->serialize($size, 'json');

        return new Response($data);
    }
}
