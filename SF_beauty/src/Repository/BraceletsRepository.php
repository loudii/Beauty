<?php

namespace App\Repository;

use App\Entity\Bracelets;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Bracelets|null find($id, $lockMode = null, $lockVersion = null)
 * @method Bracelets|null findOneBy(array $criteria, array $orderBy = null)
 * @method Bracelets[]    findAll()
 * @method Bracelets[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BraceletsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Bracelets::class);
    }

//    /**
//     * @return Bracelets[] Returns an array of Bracelets objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Bracelets
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
