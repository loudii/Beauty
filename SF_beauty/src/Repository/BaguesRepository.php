<?php

namespace App\Repository;

use App\Entity\Bagues;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Bagues|null find($id, $lockMode = null, $lockVersion = null)
 * @method Bagues|null findOneBy(array $criteria, array $orderBy = null)
 * @method Bagues[]    findAll()
 * @method Bagues[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BaguesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Bagues::class);
    }

//    /**
//     * @return Bagues[] Returns an array of Bagues objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Bagues
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
