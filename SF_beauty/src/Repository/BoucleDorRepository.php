<?php

namespace App\Repository;

use App\Entity\BoucleDor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BoucleDor|null find($id, $lockMode = null, $lockVersion = null)
 * @method BoucleDor|null findOneBy(array $criteria, array $orderBy = null)
 * @method BoucleDor[]    findAll()
 * @method BoucleDor[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BoucleDorRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BoucleDor::class);
    }

//    /**
//     * @return BoucleDor[] Returns an array of BoucleDor objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BoucleDor
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
