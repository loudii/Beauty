<?php

namespace App\Repository;

use App\Entity\Colliers;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Colliers|null find($id, $lockMode = null, $lockVersion = null)
 * @method Colliers|null findOneBy(array $criteria, array $orderBy = null)
 * @method Colliers[]    findAll()
 * @method Colliers[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ColliersRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Colliers::class);
    }

//    /**
//     * @return Colliers[] Returns an array of Colliers objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Colliers
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
