<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SizeRepository")
 */
class Size
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $size;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Bagues", mappedBy="size")
     */
    private $bagues;


    public function __construct()
    {
        $this->bagues = new ArrayCollection();
        $this->bague = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getSize(): ?int
    {
        return $this->size;
    }

    public function setSize(?int $size): self
    {
        $this->size = $size;

        return $this;
    }

    /**
     * @return Collection|Bagues[]
     */
    public function getBagues(): Collection
    {
        return $this->bagues;
    }

    public function addBague(Bagues $bague): self
    {
        if (!$this->bagues->contains($bague)) {
            $this->bagues[] = $bague;
            $bague->addSize($this);
        }

        return $this;
    }

    public function removeBague(Bagues $bague): self
    {
        if ($this->bagues->contains($bague)) {
            $this->bagues->removeElement($bague);
            $bague->removeSize($this);
        }

        return $this;
    }

    /**
     * @return Collection|Bagues[]
     */
    public function getBague(): Collection
    {
        return $this->bague;
    }
}
