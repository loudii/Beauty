<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180621130902 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE size (id INT AUTO_INCREMENT NOT NULL, size INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE boucle_dor CHANGE quantity quantity INT NOT NULL');
        $this->addSql('ALTER TABLE bracelets CHANGE quantity quantity INT NOT NULL');
        $this->addSql('ALTER TABLE bagues CHANGE quantity quantity INT NOT NULL');
        $this->addSql('ALTER TABLE colliers CHANGE quantity quantity INT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE size');
        $this->addSql('ALTER TABLE bagues CHANGE quantity quantity INT DEFAULT NULL');
        $this->addSql('ALTER TABLE boucle_dor CHANGE quantity quantity INT DEFAULT NULL');
        $this->addSql('ALTER TABLE bracelets CHANGE quantity quantity INT DEFAULT NULL');
        $this->addSql('ALTER TABLE colliers CHANGE quantity quantity INT DEFAULT NULL');
    }
}
