<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180611130358 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE boucle_dor (id INT AUTO_INCREMENT NOT NULL, reference INT NOT NULL, description VARCHAR(2000) NOT NULL, title VARCHAR(255) NOT NULL, img VARCHAR(255) NOT NULL, price INT NOT NULL, quantity INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE bracelets (id INT AUTO_INCREMENT NOT NULL, reference INT NOT NULL, description VARCHAR(2000) NOT NULL, title VARCHAR(255) NOT NULL, img VARCHAR(255) NOT NULL, price INT NOT NULL, quantity INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE bagues (id INT AUTO_INCREMENT NOT NULL, reference INT NOT NULL, size INT NOT NULL, description VARCHAR(2000) NOT NULL, title VARCHAR(255) NOT NULL, img VARCHAR(255) NOT NULL, price INT NOT NULL, quantity INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE colliers (id INT AUTO_INCREMENT NOT NULL, reference INT NOT NULL, description VARCHAR(2000) NOT NULL, titla VARCHAR(255) NOT NULL, img VARCHAR(255) NOT NULL, price INT NOT NULL, quantity INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE boucle_dor');
        $this->addSql('DROP TABLE bracelets');
        $this->addSql('DROP TABLE bagues');
        $this->addSql('DROP TABLE colliers');
    }
}
