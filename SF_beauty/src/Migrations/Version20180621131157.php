<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180621131157 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE bagues_size (bagues_id INT NOT NULL, size_id INT NOT NULL, INDEX IDX_B9573D832C7B18DD (bagues_id), INDEX IDX_B9573D83498DA827 (size_id), PRIMARY KEY(bagues_id, size_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE size_bagues (size_id INT NOT NULL, bagues_id INT NOT NULL, INDEX IDX_CD8DFEA7498DA827 (size_id), INDEX IDX_CD8DFEA72C7B18DD (bagues_id), PRIMARY KEY(size_id, bagues_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE bagues_size ADD CONSTRAINT FK_B9573D832C7B18DD FOREIGN KEY (bagues_id) REFERENCES bagues (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE bagues_size ADD CONSTRAINT FK_B9573D83498DA827 FOREIGN KEY (size_id) REFERENCES size (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE size_bagues ADD CONSTRAINT FK_CD8DFEA7498DA827 FOREIGN KEY (size_id) REFERENCES size (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE size_bagues ADD CONSTRAINT FK_CD8DFEA72C7B18DD FOREIGN KEY (bagues_id) REFERENCES bagues (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE bagues_size');
        $this->addSql('DROP TABLE size_bagues');
    }
}
