import { Routes } from '@angular/router';
import {BaguesComponent} from './bagues/bagues.component';
import {BouclesComponent} from './boucles/boucles.component';
import {BraceletsComponent} from './bracelets/bracelets.component';
import {ColliersComponent} from './colliers/colliers.component';
import {HomeComponent} from './home/home.component';
import {BagueComponent} from './bague/bague.component';
import {BoucleComponent} from './boucle/boucle.component';
import {BraceletComponent} from './bracelet/bracelet.component';
import {CollierComponent} from './collier/collier.component';
import {PanierComponent} from './panier/panier.component';

export const routes: Routes = [
  {path: 'bagues', component: BaguesComponent},
  {path: 'bague/:id', component: BagueComponent},
  {path: 'boucles', component: BouclesComponent},
  {path: 'boucle/:id', component: BoucleComponent},
  {path: 'bracelets', component: BraceletsComponent},
  {path: 'bracelet/:id', component: BraceletComponent},
  {path: 'colliers', component: ColliersComponent},
  {path: 'collier/:id', component: CollierComponent},
  {path: '', component: HomeComponent},
  {path: 'cart', component: PanierComponent},
]
