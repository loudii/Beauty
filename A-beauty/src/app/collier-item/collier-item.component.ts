import {Component, Input, OnInit} from '@angular/core';
import {BijouxService} from '../services/bijoux.service';

@Component({
  selector: 'app-collier-item',
  templateUrl: './collier-item.component.html',
  styleUrls: ['./collier-item.component.scss']
})
export class CollierItemComponent implements OnInit {

  @Input() public img: string;
  @Input() public titla: string;
  @Input() public price: number;
  @Input() public reference: number;
  @Input() public quantity: number;
  @Input() public description: string;
  @Input() public id: string;

  constructor(private bijouxService: BijouxService) { }

  ngOnInit() {
  }

}
