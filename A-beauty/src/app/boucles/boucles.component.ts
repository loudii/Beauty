import { Component, OnInit } from '@angular/core';
import {BijouxService} from '../services/bijoux.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-boucles',
  templateUrl: './boucles.component.html',
  styleUrls: ['./boucles.component.scss']
})
export class BouclesComponent implements OnInit {

  public boucles: any[] = [];
  img: string;
  title: string;
  price: number;

  constructor(private bijouxService: BijouxService, private router: Router){ }

  ngOnInit() {
    console.log(this.bijouxService)
    this.bijouxService.bouclesSubject.subscribe(
      (boucles) => this.boucles = boucles
    )
    this.bijouxService.getBoucles();
  }

}
