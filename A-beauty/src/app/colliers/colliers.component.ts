import { Component, OnInit } from '@angular/core';
import {BijouxService} from '../services/bijoux.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-colliers',
  templateUrl: './colliers.component.html',
  styleUrls: ['./colliers.component.scss']
})
export class ColliersComponent implements OnInit {

  public colliers: any[] = [];
  img: string;
  titla: string;
  price: number;

  constructor(private bijouxService: BijouxService, private router: Router){ }

  ngOnInit() {
    console.log(this.bijouxService)
    this.bijouxService.colliersSubject.subscribe(
      (colliers) => this.colliers = colliers
    )
    this.bijouxService.getColliers();
  }


}
