import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { routes } from './routes';
import {FormsModule} from '@angular/forms';


import { AppComponent } from './app.component';
import { BaguesComponent } from './bagues/bagues.component';
import { BagueItemComponent } from './bague-item/bague-item.component';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {BijouxService} from './services/bijoux.service';
import { BouclesComponent } from './boucles/boucles.component';
import { BoucleItemComponent } from './boucle-item/boucle-item.component';
import { BraceletsComponent } from './bracelets/bracelets.component';
import { BraceletItemComponent } from './bracelet-item/bracelet-item.component';
import { ColliersComponent } from './colliers/colliers.component';
import { CollierItemComponent } from './collier-item/collier-item.component';
import { HomeComponent } from './home/home.component';
import { BagueComponent } from './bague/bague.component';
import { BoucleComponent } from './boucle/boucle.component';
import { CollierComponent } from './collier/collier.component';
import { BraceletComponent } from './bracelet/bracelet.component';
import { PanierComponent } from './panier/panier.component';


@NgModule({
  declarations: [
    AppComponent,
    BaguesComponent,
    BagueItemComponent,
    BouclesComponent,
    BoucleItemComponent,
    BraceletsComponent,
    BraceletItemComponent,
    ColliersComponent,
    CollierItemComponent,
    HomeComponent,
    BagueComponent,
    BoucleComponent,
    CollierComponent,
    BraceletComponent,
    PanierComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    FormsModule,
  ],
  providers: [
    BijouxService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
