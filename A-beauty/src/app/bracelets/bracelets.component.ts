import { Component, OnInit } from '@angular/core';
import {BijouxService} from '../services/bijoux.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-bracelets',
  templateUrl: './bracelets.component.html',
  styleUrls: ['./bracelets.component.scss']
})
export class BraceletsComponent implements OnInit {


  public bracelets: any[] = [];
  img: string;
  title: string;
  price: number;

  constructor(private bijouxService: BijouxService, private router: Router){ }

  ngOnInit() {
    console.log(this.bijouxService)
    this.bijouxService.braceletsSubject.subscribe(
      (bracelets) => this.bracelets = bracelets
    )
    this.bijouxService.getBracelets();
  }

}
