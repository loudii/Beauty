import {Component, Input, OnInit} from '@angular/core';
import {BijouxService} from '../services/bijoux.service';

@Component({
  selector: 'app-bracelet-item',
  templateUrl: './bracelet-item.component.html',
  styleUrls: ['./bracelet-item.component.scss']
})
export class BraceletItemComponent implements OnInit {

  @Input() public img: string;
  @Input() public title: string;
  @Input() public price: number;
  @Input() public reference: number;
  @Input() public quantity: number;
  @Input() public description: string;
  @Input() public id: string;

  constructor(private bijouxService: BijouxService) { }

  ngOnInit() {
  }

}
