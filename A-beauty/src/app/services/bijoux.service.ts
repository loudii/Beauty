import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs/Subject';

@Injectable()

export class BijouxService {

  constructor(private http: HttpClient) { }

  private readonly API: string = 'http://127.0.0.1:8000';

  private bagues: any[] = []

  public baguesSubject: Subject<any>  = new Subject<any>()

  emitBagues() {
    this.baguesSubject.next(this.bagues);
  }

  /*récup bijoux */

  getBagues() {
    this.http.get<any>(`${this.API}/bagues`)
      .subscribe(
        (bagues) => {
          this.bagues = bagues;
          this.emitBagues();
        }
        // error => { console.error('AdService.getAds(): ', error) },
        // () => { console.info('AdService.getAds(): complete') }
      );
  }

  private boucles: any[] = []
  public bouclesSubject: Subject<any>  = new Subject<any>()

  emitBoucles() {
    this.bouclesSubject.next(this.boucles);
  }

  getBoucles() {
    this.http.get<any>(`${this.API}/boucles`)
      .subscribe(
        (boucles) => {
          this.boucles = boucles;
          this.emitBoucles();
        }
        // error => { console.error('AdService.getAds(): ', error) },
        // () => { console.info('AdService.getAds(): complete') }
      );
  }

  private bracelets: any[] = []
  public braceletsSubject: Subject<any>  = new Subject<any>()

  emitBracelets() {
    this.braceletsSubject.next(this.bracelets);
  }

  getBracelets() {
    this.http.get<any>(`${this.API}/bracelets`)
      .subscribe(
        (bracelets) => {
          this.bracelets = bracelets;
          this.emitBracelets();
        }
        // error => { console.error('AdService.getAds(): ', error) },
        // () => { console.info('AdService.getAds(): complete') }
      );
  }

  private colliers: any[] = []
  public colliersSubject: Subject<any>  = new Subject<any>()

  emitColliers() {
    this.colliersSubject.next(this.colliers);
  }

  getColliers() {
    this.http.get<any>(`${this.API}/colliers`)
      .subscribe(
        (colliers) => {
          this.colliers = colliers;
          this.emitColliers();
        }
        // error => { console.error('AdService.getAds(): ', error) },
        // () => { console.info('AdService.getAds(): complete') }
      );
  }

  /*récup bijoux by ID*/

  findById(id: string) {
    return this.bagues.find((bague) => String(bague.id) === id)
  }

  getBague(id: string) {
    this.http.get<any>(`${this.API}/bague/${id}`)
      .subscribe(
        bague => {
          /*console.log(bagues.id)
          console.log(bagues.price)*/
          this.insertOrUpdate(bague)
          this.emitBagues();
        }
        /*error => { console.error('AdService.getAds(): ', error) },
        () => { console.info('AdService.getAds(): complete') }*/
      )
  }

  insertOrUpdate(newBague) {
    let bague = this.findById(newBague.id)
    if (bague) {
      bague = newBague
    }
    else {
      this.bagues.push(newBague)
    }
  }
  findCollierById(id: string) {
    return this.colliers.find((collier) => String(collier.id) === id)
  }
  getCollier(id: string) {
    this.http.get<any>(`${this.API}/collier/${id}`)
      .subscribe(
        collier => {
          /*console.log(bagues.id)
          console.log(bagues.price)*/
          this.insertOrUpdateCollier(collier)
          this.emitColliers();
        }
        /*error => { console.error('AdService.getAds(): ', error) },
        () => { console.info('AdService.getAds(): complete') }*/
      )
  }

  insertOrUpdateCollier(newCollier) {
    let collier = this.findCollierById(newCollier.id)
    if (collier) {
      collier = newCollier
    }
    else {
      this.colliers.push(newCollier)
    }
  }


  findBraceletById(id: string) {
    return this.bracelets.find((bracelet) => String(bracelet.id) === id)
  }
  getBracelet(id: string) {
    this.http.get<any>(`${this.API}/bracelet/${id}`)
      .subscribe(
        bracelet => {
          this.insertOrUpdateBracelet(bracelet)
          this.emitBracelets();
        }
        /*error => { console.error('AdService.getAds(): ', error) },
        () => { console.info('AdService.getAds(): complete') }*/
      )
  }

  insertOrUpdateBracelet(newBracelet) {
    let bracelet = this.findBraceletById(newBracelet.id)
    if (bracelet) {
      bracelet = newBracelet
    }
    else {
      this.bracelets.push(newBracelet)
    }
  }

  findBoucleById(id: string) {
    return this.boucles.find((boucle) => String(boucle.id) === id)
  }
  getBoucle(id: string) {
    this.http.get<any>(`${this.API}/boucle/${id}`)
      .subscribe(
        boucle => {
          this.insertOrUpdateBoucle(boucle)
          this.emitBoucles();
        }
        /*error => { console.error('AdService.getAds(): ', error) },
        () => { console.info('AdService.getAds(): complete') }*/
      )
  }

  insertOrUpdateBoucle(newBoucle) {
    let boucle = this.findBoucleById(newBoucle.id)
    if (boucle) {
      boucle = newBoucle
    }
    else {
      this.boucles.push(newBoucle)
    }
  }

  addSize(size: number){
    const observable = this.http.post<{name}>(`${this.API}/size`, JSON.stringify({ 'size' : size}));
    const observer = {
      next: () => {
        console.log('yeaaah')
      },
      error: (error) => { console.log(error) },
      complete: () => {}
    }

    observable.subscribe(observer);
  }

}
