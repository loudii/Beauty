import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {BijouxService} from '../services/bijoux.service';

@Component({
  selector: 'app-collier',
  templateUrl: './collier.component.html',
  styleUrls: ['./collier.component.scss']
})
export class CollierComponent implements OnInit {

  public id: string;
  public collier: any;

  constructor(private route: ActivatedRoute, private bijouxService: BijouxService) {
  }

  ngOnInit() {
    this.id = String(this.route.snapshot.params['id'])
    this.bijouxService.colliersSubject.subscribe(
      (colliers) => {
        this.collier = this.bijouxService.findCollierById(this.id)
      }
    )
    this.bijouxService.getCollier(this.id)
  }
}
