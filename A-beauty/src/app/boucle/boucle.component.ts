///<reference path="../../../node_modules/@angular/core/src/metadata/directives.d.ts"/>
import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {BijouxService} from '../services/bijoux.service';

@Component({
  selector: 'app-boucle',
  templateUrl: './boucle.component.html',
  styleUrls: ['./boucle.component.scss']
})
export class BoucleComponent implements OnInit {

  public id: string;
  public boucle: any;

  constructor(private route: ActivatedRoute, private bijouxService: BijouxService) {
  }

  ngOnInit() {
    this.id = String(this.route.snapshot.params['id'])
    this.bijouxService.bouclesSubject.subscribe(
      (boucles) => {
        this.boucle = this.bijouxService.findBoucleById(this.id)
      }
    )
    this.bijouxService.getBoucle(this.id)
  }

}
