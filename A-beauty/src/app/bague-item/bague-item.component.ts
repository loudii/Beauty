import {Component, Input, OnInit} from '@angular/core';
import {BijouxService} from '../services/bijoux.service';

@Component({
  selector: 'app-bague-item',
  templateUrl: './bague-item.component.html',
  styleUrls: ['./bague-item.component.scss']
})
export class BagueItemComponent implements OnInit {

  @Input() public img: string;
  @Input() public title: string;
  @Input() public price: number;
  @Input() public size: number;
  @Input() public reference: number;
  @Input() public quantity: number;
  @Input() public description: string;
  @Input() public id: string;

  constructor(private bijouxService: BijouxService) { }

  ngOnInit() {
  }

}
