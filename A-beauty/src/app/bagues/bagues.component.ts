import { Component, OnInit } from '@angular/core';
import {BijouxService} from '../services/bijoux.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-bagues',
  templateUrl: './bagues.component.html',
  styleUrls: ['./bagues.component.scss']
})
export class BaguesComponent implements OnInit {

  public bagues: any[] = [];

  constructor(private bijouxService: BijouxService, private router: Router){ }

  ngOnInit() {
    console.log(this.bijouxService)
    this.bijouxService.baguesSubject.subscribe(
      (bagues) => this.bagues = bagues
    )
    this.bijouxService.getBagues();
  }
}
