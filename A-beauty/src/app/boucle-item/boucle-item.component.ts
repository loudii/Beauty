import {Component, Input, OnInit} from '@angular/core';
import {BijouxService} from '../services/bijoux.service';

@Component({
  selector: 'app-boucle-item',
  templateUrl: './boucle-item.component.html',
  styleUrls: ['./boucle-item.component.scss']
})
export class BoucleItemComponent implements OnInit {

  @Input() public img: string;
  @Input() public title: string;
  @Input() public price: number;
  @Input() public reference: number;
  @Input() public quantity: number;
  @Input() public description: string;
  @Input() public id: string;

  constructor(private bijouxService: BijouxService) { }

  ngOnInit() {
  }

}
