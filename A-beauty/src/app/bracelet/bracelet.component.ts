import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {BijouxService} from '../services/bijoux.service';

@Component({
  selector: 'app-bracelet',
  templateUrl: './bracelet.component.html',
  styleUrls: ['./bracelet.component.scss']
})
export class BraceletComponent implements OnInit {

  public id: string;
  public bracelet: any;

  constructor(private route: ActivatedRoute, private bijouxService: BijouxService) {
  }

  ngOnInit() {
    this.id = String(this.route.snapshot.params['id'])
    this.bijouxService.braceletsSubject.subscribe(
      (bracelets) => {
        this.bracelet = this.bijouxService.findBraceletById(this.id)
      }
    )
    this.bijouxService.getBracelet(this.id)
  }

}
