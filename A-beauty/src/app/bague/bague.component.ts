import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {BijouxService} from '../services/bijoux.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-bague',
  templateUrl: './bague.component.html',
  styleUrls: ['./bague.component.scss']
})
export class BagueComponent implements OnInit {

  public id: string;
  public bague: any;

  bagueSizeValue = null;
  options = [48, 49 , 50, 51, 52, 53, 54, 55, 56];
  size: number;

  constructor(private route: ActivatedRoute, private bijouxService: BijouxService) {
  }

  ngOnInit() {
    this.id = String(this.route.snapshot.params['id'])
    this.bijouxService.baguesSubject.subscribe(
      (bagues) => {
        this.bague = this.bijouxService.findById(this.id)
      }
    )
    this.bijouxService.getBague(this.id)
  }


  onSizeChange(value: string) {
    console.log(value);
    this.bagueSizeValue = value;
  }
  submitSize(form: NgForm){
    const size = form.value['size'];
    this.bijouxService.addSize(size);
  }
}
